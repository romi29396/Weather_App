variable "region" {
  description = "AWS region"
  default     = "us-east-1"
}

variable "cluster_name" {
  description = "The name of the EKS cluster"
  default     = "my-cluster"
}

variable "cluster_version" {
  description = "The version of Kubernetes for the EKS cluster"
  default     = "1.29"
}

variable "environment" {
  description = "Environment tag"
  default     = "dev"
}

variable "ec2_instance_type" {
  description = "EC2 instance type"
  default     = "t2.micro"
}

# variable "key_name" {
#   description = "Key name for SSH access"
#   default     = "Prod_Keys"
# }

