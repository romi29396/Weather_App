provider "aws" {
  region = "us-east-1"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "4.0.0"

  name = "eks-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["us-east-1a", "us-east-1b"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24"]

  enable_nat_gateway = true
  single_nat_gateway = true

  tags = {
    Terraform   = "true"
    Environment = var.environment
  }
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "19.13.1"

  cluster_name    = var.cluster_name
  cluster_version = var.cluster_version
  subnet_ids      = module.vpc.private_subnets
  vpc_id          = module.vpc.vpc_id
  cluster_endpoint_public_access = true

  eks_managed_node_groups = {
    eks_nodes = {
      name            = "eks-nodes"
      instance_types   = ["t2.micro"]
      desired_capacity = 2
      min_capacity     = 2
      max_capacity     = 2
      desired_size = 2
      min_size     = 2
      max_size     = 3
      # key_name         = var.key_name
    }
  }

  tags = {
    Environment = var.environment
  }
}

# terraform { 
#   backend "s3" {
#     bucket         = "terraform-romi"
#     key            = "global/s3/terraform.tfstate"
#     region         = "us-east-1"
#     encrypt        = true
#   }
# }

# resource "aws_security_group" "alb" {
#   name        = "${var.environment}-alb-sg"
#   description = "Security group for ALB"
#   vpc_id      = module.vpc.vpc_id

#   ingress {
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   ingress {
#     from_port   = 443
#     to_port     = 443
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   tags = {
#     Environment = var.environment
#   }
# }

# resource "aws_lb" "alb" {
#   name               = "${var.environment}-alb"
#   internal           = false
#   load_balancer_type = "application"
#   security_groups    = [aws_security_group.alb.id]
#   subnets            = module.vpc.public_subnets

#   enable_deletion_protection = false

#   tags = {
#     Environment = var.environment
#   }
# }

# resource "aws_lb_target_group" "tg" {
#   name     = "${var.environment}-tg"
#   port     = 80
#   protocol = "HTTP"
#   vpc_id   = module.vpc.vpc_id

#   health_check {
#     enabled             = true
#     interval            = 30
#     path                = "/"
#     protocol            = "HTTP"
#     timeout             = 5
#     healthy_threshold   = 2
#     unhealthy_threshold = 2
#   }

#   tags = {
#     Environment = var.environment
#   }
# }



